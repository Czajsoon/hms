export const routing = {
  login: "logowanie",
  rooms: "pokoje",
  management: "zarzadzanie-kontami",
  twoFactorAuth: "two-factor-auth",
  changePassword: "change-password"
}

export default routing;

import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../../shared/auth/auth.service";
import {AddRoomComponent} from "../add-room/add-room.component";
import {RoomDetails, RoomDetailsPage, RoomStatus, RoomStatusName} from "../../models/room";
import {Page} from "../../models/page";
import {RoomRestService} from "../../rest-services/room-rest.service";

@Component({
  selector: 'hms-rooms',
  templateUrl: './room.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {

  @ViewChild(AddRoomComponent, {static: true}) addRoom: AddRoomComponent;
  roomsPage: RoomDetailsPage;
  page: Page = {page: 0};
  searchParams: RoomDetails = {} as RoomDetails;
  roomStatusNames: RoomStatusName[]=[];

  constructor(public auth: AuthService, private roomRest: RoomRestService) {
    this.buildRoomStatusNames();
  }

  ngOnInit(): void {
    this.loadRooms();
  }



  showAddRoom() {
    this.addRoom.changeVisibleModal();
  }

  loadRooms(): void {
    this.roomRest.getRooms(null, 0).subscribe(rooms => {this.roomsPage = rooms; console.log(rooms)})
  }
  callPage($event: any) {
    this.page = $event
    this.roomRest.getRooms(this.searchParams, this.page.page).subscribe(rooms => {
      console.log(rooms);
      this.roomsPage = rooms;
    })
  }

  search(event: any) {
    this.searchParams = event;
    console.log(this.searchParams);
    this.roomRest.getRooms(event, 0).subscribe(rooms => {
      this.roomsPage = rooms;
      console.log(rooms);
    });
  }

  buildRoomStatusNames() {
    this.roomStatusNames.push(
      {name: "Dostępny", value: RoomStatus.AVAILABLE},
      {name: "Zajęty", value: RoomStatus.OCCUPIED},
      {name: "Do przygotowania", value: RoomStatus.TO_PREPARE},
      {name: "Zawieszony", value: RoomStatus.DISABLED}
    )
  }
}

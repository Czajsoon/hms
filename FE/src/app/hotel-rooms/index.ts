import {RoomsComponent} from "./room/rooms.component";
import {AddRoomComponent} from "./add-room/add-room.component";
import {RoomSearchFormComponent} from "./room-search-form/room-search-form.component";
import {RoomViewComponent} from "./room-view/room-view.component";
import {SingleRoomViewComponent} from "./single-room-view/single-room-view.component";
import {ChangeRoomStatusComponent} from "./single-room-view/change-room-status/change-room-status.component";

export const COMPONENTS = [
  RoomsComponent,
  AddRoomComponent,
  RoomSearchFormComponent,
  RoomViewComponent,
  SingleRoomViewComponent,
  ChangeRoomStatusComponent
]

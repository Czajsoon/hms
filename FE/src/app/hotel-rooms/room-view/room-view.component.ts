import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Page} from "../../models/page";
import {RoomDetailsPage, RoomStatus} from "../../models/room";

@Component({
  selector: 'hms-room-view',
  templateUrl: './room-view.component.html',
  styleUrls: ['./room-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoomViewComponent implements OnInit {

  @Input() roomsPage: RoomDetailsPage;
  @Output() changePageEvent: EventEmitter<Page> = new EventEmitter<Page>();

  constructor() { }

  ngOnInit(): void {
  }

  getBalconyAvailableStringValue(balconyAvailable:boolean): string{
    if(balconyAvailable)
      return "TAK"
    else
      return "NIE"
  }

  getRoomStatusValue(status: RoomStatus): string{
    if(status==="DISABLED")
      return "Niedostępny"
    else if(status==="AVAILABLE")
      return "Dostępny"
    else if(status==="TO_PREPARE")
      return "Do przygotowania"
    else
      return "Zajęty"
  }

  changePage(event: any) {
    this.changePageEvent.emit({page: event.page})
  }

}

import {Component,OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BalconyAvailable} from "../../models/room";
import {RoomRestService} from "../../rest-services/room-rest.service";


@Component({
  selector: 'hms-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.scss']
})
export class AddRoomComponent implements OnInit {
  balconyAvailable: BalconyAvailable[]=[];

  displayModal: boolean = false;
  form: FormGroup;

  constructor(private fb: FormBuilder, private rest: RoomRestService) {
    this.buildForm();

  }

  ngOnInit(): void {
    this.balconyAvailable = [{ label: 'TAK', value: true },{ label: 'NIE', value: false }];
  }

  changeVisibleModal() {
    this.displayModal = !this.displayModal;
  }

  addRoom(){
    this.rest.addRoom(this.form.getRawValue()).subscribe(room=>{
      console.log("dodano"+room);
      this.changeVisibleModal()
    });
  }

  buildForm(): void {
    this.form = this.fb.group({
      level: [null, Validators.required],
      pricePerNight: [null, Validators.required],
      bedsNumber: [null, Validators.required],
      balconyAvailable: [this.balconyAvailable, Validators.required],
    })
  }





}

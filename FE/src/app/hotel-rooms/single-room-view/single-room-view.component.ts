import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {RoomDetails} from "../../models/room";
import {ChangeRoomStatusComponent} from "./change-room-status/change-room-status.component";

@Component({
  selector: 'hms-single-room-view',
  templateUrl: './single-room-view.component.html',
  styleUrls: ['./single-room-view.component.scss']
})
export class SingleRoomViewComponent implements OnInit {

  @Input() room: RoomDetails
  @ViewChild(ChangeRoomStatusComponent, {static: true}) changeRoomStatus: ChangeRoomStatusComponent;

  constructor() {
  }

  ngOnInit(): void {
  }


}

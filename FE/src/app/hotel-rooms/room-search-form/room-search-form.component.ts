import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AccountDetails, RoleName} from "../../models/account";
import {BalconyAvailable, RoomDetails, RoomStatusName} from "../../models/room";

@Component({
  selector: 'hms-room-search-form',
  templateUrl: './room-search-form.component.html',
  styleUrls: ['./room-search-form.component.scss']
})
export class RoomSearchFormComponent implements OnInit {

  form: FormGroup;

  @Output() searchParams: EventEmitter<RoomDetails> = new EventEmitter<RoomDetails>();
  @Input() roomStatusNames: RoomStatusName[] = [];
  balconyAvailable: BalconyAvailable[]=[];
  constructor(private fb:  FormBuilder) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.balconyAvailable = [{ label: 'TAK', value: true },{ label: 'NIE', value: false }];
  }

  buildForm(): void {
    this.form = this.fb.group({
      balconyAvailable: this.balconyAvailable,
      level: null,
      roomStatus: null,
      pricePerNight: null,
      bedsNumber: null
    })
  }

  search() {
    this.searchParams.emit(this.form.value )
  }

  clearFilters() {
    this.form.reset();
  }

}

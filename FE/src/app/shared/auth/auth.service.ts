import {Injectable} from '@angular/core';
import {AccountDetail, Role} from "../../models/account";
import {AccountRestService} from "../../rest-services/account-rest.service";
import {TwoFactorAccountConfig} from "../../models/TwoFactor";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  accountDetails: AccountDetail = {} as AccountDetail;
  private _twoFaValidated: boolean = false;
  private _twoFA: TwoFactorAccountConfig = {
    is2FaConfigured: false,
    isUsing2FA: false
  }

  constructor(private accountRest: AccountRestService) {
  }

  public getDetails() {
    return this.accountRest.getAccountDetail();
  }

  get details(): AccountDetail {
    return this.accountDetails;
  }

  set details(details: AccountDetail) {
    this.accountDetails = details;
  }

  get twoFaValidated() {
    return this._twoFaValidated
  }

  set twoFaValidated(validated: boolean) {
    this._twoFaValidated = validated;
  }

  get isAdmin() {
    return Role.ADMIN === this.accountDetails.role;
  }

  set twoFA(twoFA: TwoFactorAccountConfig) {
    this._twoFA = twoFA;
  }

  get twoFA() {
    return this._twoFA;
  }

}

import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of, switchMap, tap} from 'rxjs';
import {TokenHandlerService} from "../token-handler/token-handler.service";
import {AuthService} from "../auth/auth.service";
import routing from "../../../environments/routing";
import {AccountDetail} from "../../models/account";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private tokenHandler: TokenHandlerService,
              private auth: AuthService,
              private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.tokenHandler.isTokenAvailable) {
      return this.loadDetails().pipe(
        switchMap(() => {
          if (this.auth.twoFaValidated) {
            return of(true);
          } else {
            this.router.navigate([routing.login]);
            return of(false);
          }
        })
      );
    } else {
      this.router.navigate([routing.login]);
      return of(false);
    }
  }

  private loadDetails(): Observable<AccountDetail> {
    if (Object.entries(this.auth.accountDetails).length === 0) {
      return this.auth.getDetails().pipe(
        tap({
            next: (details) => {
              this.auth.accountDetails = details;
              if (details.isUsing2FA) {
                this.auth.twoFaValidated = this.tokenHandler.isTwoFactorAvailable;
              } else {
                this.auth.twoFaValidated = !details.isUsing2FA;
              }
            }
          }
        ))
    } else {
      return of(undefined);
    }
  }

}

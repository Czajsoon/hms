import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from "../auth/auth.service";
import {Role} from "../../models/account";
import {LocationStrategy} from "@angular/common";
import {ErrorMessage, ErrorTitle} from "../exceptions-handlers/messages";
import {MessageService} from "primeng/api";
import routing from "../../../environments/routing";

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private auth: AuthService,
              private location: LocationStrategy,
              private router: Router,
              private messageService: MessageService) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.auth.details.role === Role.ADMIN) {
      return true;
    } else {
      this.router.navigate([routing.rooms])
        .finally(() => {
          this.showUnauthorizedPopup();
        })
      return false;
    }
  }

  showUnauthorizedPopup() {
    this.messageService.add({
      severity: 'error',
      summary: ErrorTitle.NO_PERMISSIONS,
      detail: ErrorMessage.NO_PERMISSIONS
    })
  }

}

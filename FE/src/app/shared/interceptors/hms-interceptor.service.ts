import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {finalize, Observable, tap} from "rxjs";
import {LoadingService} from "../loading/loading.service";
import {TokenHandlerService} from "../token-handler/token-handler.service";
import apiUrl from "../../../environments/apiurl";

@Injectable()
export class HMSInterceptor implements HttpInterceptor {

  constructor(private loading: LoadingService,
              private tokenHandler: TokenHandlerService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(this.handleToken(req)).pipe(
      this.handleLoader()
    );
  }

  handleToken(req: HttpRequest<any>) {
    if (!req.url.endsWith('/authenticate/login')) {
      req = req.clone({
        setHeaders: this.tokenHandler.tokenHeader,
        url: `${apiUrl}${req.url}`
      });
      return req;
    } else {
      req = req.clone({
        url: `${apiUrl}${req.url}`
      });
      return req;
    }
  }

  handleLoader() {
    return next$ => next$.pipe(
      tap(() => {
        this.loading.show();
      }),
      finalize(() => {
        this.loading.hide();
      })
    )
  }

}

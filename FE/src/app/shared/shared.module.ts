import {NgModule} from '@angular/core';
import {PIPES, STYLE_MODULES} from "./index";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  imports: [
    ...STYLE_MODULES,
    ReactiveFormsModule
  ],
  exports: [
    ...STYLE_MODULES,
    ...PIPES,
    ReactiveFormsModule
  ],
  declarations: [
    ...PIPES
  ]
})
export class SharedModule {
}

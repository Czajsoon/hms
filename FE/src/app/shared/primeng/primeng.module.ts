import {NgModule} from '@angular/core';
import {MessageService} from "primeng/api";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ButtonModule} from "primeng/button";
import {InputTextModule} from "primeng/inputtext";
import {PasswordModule} from "primeng/password";
import {RippleModule} from "primeng/ripple";
import {TooltipModule} from "primeng/tooltip";
import {ToastModule} from "primeng/toast";
import {DialogModule} from "primeng/dialog";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {TableModule} from "primeng/table";
import {PaginatorModule} from "primeng/paginator";
import {AccordionModule} from "primeng/accordion";
import {InputSwitchModule} from "primeng/inputswitch";

export const PRIMENG_MODULES = [
  BrowserModule,
  BrowserAnimationsModule,
  ButtonModule,
  InputTextModule,
  PasswordModule,
  RippleModule,
  TooltipModule,
  ToastModule,
  DialogModule,
  ProgressSpinnerModule,
  TableModule,
  PaginatorModule,
  AccordionModule,
  InputSwitchModule
]

@NgModule({
  declarations: [],
  imports: [],
  exports: [PRIMENG_MODULES],
  providers: [MessageService]
})
export class PrimengModule {
}

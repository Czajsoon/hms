import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {tap} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  constructor(public router: Router) {
  }

  public navigate(url: string) {
    return response$ => response$.pipe(
      tap({
        next: () => this.router.navigate([url])
      })
    )
  }
}

export enum SuccessTitle {
  SUCCESS = "Operacja wykonana pomyślnie!"
}

export enum SuccessMessage {
  SUCCESS = "Wykonano operację."
}

export enum ErrorMessage {
  ERROR = "Wystąpił błąd: ",
  NO_PERMISSIONS = "Nie posiadasz uprawnień do tego widoku."
}

export enum ErrorTitle {
  ERROR = "Wystąpił błąd!",
  NO_PERMISSIONS = "Brak wymaganych uprawnień!"
}

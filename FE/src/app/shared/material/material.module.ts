import {NgModule} from '@angular/core';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatSidenavModule} from "@angular/material/sidenav";

export const MATERIAL_MODULES = [
  MatToolbarModule,
  MatSidenavModule
]

@NgModule({
  declarations: [],
  imports: [],
  exports: [MATERIAL_MODULES]
})
export class MaterialModule {
}

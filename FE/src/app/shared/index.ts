import {MaterialModule} from "./material/material.module";
import {PrimengModule} from "./primeng/primeng.module";
import {EnumRoleResolverPipe} from "./pipes/enum-role-resolver.pipe";
import {YesNoPipe} from "./pipes/yes-no.pipe";
import {RoomStatusPipe} from "./pipes/room-status.pipe";

export const STYLE_MODULES = [
  MaterialModule,
  PrimengModule
]

export const PIPES = [
  EnumRoleResolverPipe,
  YesNoPipe,
  RoomStatusPipe
]

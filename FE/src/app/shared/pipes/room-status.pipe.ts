import {Pipe, PipeTransform} from '@angular/core';
import {RoomStatus} from "../../models/room";

@Pipe({
  name: 'roomStatus'
})
export class RoomStatusPipe implements PipeTransform {

  transform(value: RoomStatus): string {
    switch (value) {
      case RoomStatus.AVAILABLE:
        return 'Dostępny'
      case RoomStatus.DISABLED:
        return 'Wyłączony z użytku'
      case RoomStatus.OCCUPIED:
        return 'Wynajmowany'
      case RoomStatus.TO_PREPARE:
        return 'Do przygotowania'
      default:
        return ''
    }

  }

}

import {Injectable} from '@angular/core';
import {CookieService} from "ngx-cookie-service";
import {tap} from "rxjs";
import {Router} from "@angular/router";
import routing from "../../../environments/routing";
import {AuthService} from "../auth/auth.service";
import {TwoFactorAccountConfig} from "../../models/TwoFactor";
import {AccountDetail, Role} from "../../models/account";

@Injectable({
  providedIn: 'root'
})
export class TokenHandlerService {

  constructor(private cookieService: CookieService,
              private auth: AuthService,
              private router: Router) {
  }

  public handleToken() {
    return response$ => response$.pipe(
      tap({
        next: (data: any) => {
          this.cookieService.set("token", data.token)
        }
      })
    )
  }

  public setTwoFaValidatedOnSuccess() {
    return response$ => response$.pipe(
      tap({
        next: () => {
          this.auth.twoFaValidated = true;
          this.cookieService.set("two-factor-validated", "true")
        }
      })
    )
  }

  set twoFaValid(valid: boolean) {
    this.auth.twoFaValidated = valid;
  }

  get twoFA() {
    return this.auth.twoFA
  }

  set twoFA(twoFA: TwoFactorAccountConfig) {
    this.auth.twoFA = twoFA
  }

  set details(details: AccountDetail) {
    this.auth.accountDetails = details;
  }

  get tokenHeader() {
    return {
      "Authorization": `Bearer ${this.cookieService.get("token")}`
    }
  }

  get isTokenAvailable(): boolean {
    return this.cookieService.check("token");
  }

  get isTwoFactorAvailable(): boolean {
    return this.cookieService.check("two-factor-validated") && this.cookieService.get("two-factor-validated") === "true";
  }

  set setTwoFactorAvailable(value: string) {
    this.cookieService.set("two-factor-validated", value)
  }

  public logout() {
    this.auth.details = {} as AccountDetail
    this.auth.twoFA = {
      isUsing2FA: false,
      is2FaConfigured: false
    }
    this.auth.twoFaValidated = false;
    this.cookieService.delete("token")
    this.router.navigate([routing.login])
  }

  get details(): AccountDetail {
    return this.auth.accountDetails;
  }

  get isAdmin() {
    return Role.ADMIN === this.auth.accountDetails.role;
  }
}

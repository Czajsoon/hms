import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AccountDetailsPage, Role} from "../../models/account";
import {Page} from "../../models/page";
import {ChangeTwoFactor} from "../../models/TwoFactor";

@Component({
  selector: 'hms-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountsComponent implements OnInit {
  @Input() accountsPage: AccountDetailsPage;
  @Output() changePageEvent: EventEmitter<Page> = new EventEmitter<Page>();
  @Output() deleteAccountId: EventEmitter<string> = new EventEmitter<string>();
  @Output() change2FAEvent: EventEmitter<ChangeTwoFactor> = new EventEmitter<ChangeTwoFactor>()
  @Output() reset2FAEvent: EventEmitter<string> = new EventEmitter<string>()
  role = Role;

  constructor() {
  }

  ngOnInit(): void {

  }

  changePage(event: any) {
    this.changePageEvent.emit({page: event.page})
  }

  deleteAccount(accountId: string) {
    this.deleteAccountId.emit(accountId);
  }

  change2FA($event: any, accountId: string) {
    this.change2FAEvent.emit({value: $event.checked, accountId: accountId})
  }

  reset2FA(accountId: string) {
    this.reset2FAEvent.emit(accountId);
  }
}

import {Component, OnInit, ViewChild} from '@angular/core';
import {AddAccountComponent} from "../add-account/add-account.component";
import {AccountRestService} from "../../rest-services/account-rest.service";
import {AccountDetails, AccountDetailsPage, AccountRegister, Role, RoleName} from "../../models/account";
import {Page} from "../../models/page";
import {tap} from "rxjs";
import {ChangeTwoFactor} from "../../models/TwoFactor";

@Component({
  selector: 'hms-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss']
})
export class ManagementComponent implements OnInit {

  @ViewChild(AddAccountComponent, {static: true}) addAccount: AddAccountComponent;
  accountsPage: AccountDetailsPage;
  page: Page = {page: 0};
  searchParams: AccountDetails = {} as AccountDetails;
  availableRoles: RoleName[] = [];


  constructor(private accountRest: AccountRestService) {
    this.buildAvailableRoles();
  }

  ngOnInit(): void {
    this.loadAccounts()
  }

  loadAccounts(): void {
    this.accountRest.getAccounts(null, 0).subscribe(accounts => this.accountsPage = accounts)
  }

  showAddRoom() {
    this.addAccount.changeVisibleModal();
  }

  callPage($event: any) {
    this.page = $event
    this.accountRest.getAccounts(this.searchParams, this.page.page).subscribe(accounts => this.accountsPage = accounts)
  }

  search(event: any) {
    this.searchParams = event;
    this.accountRest.getAccounts(event, 0).subscribe(accounts => this.accountsPage = accounts)
  }

  buildAvailableRoles() {
    this.availableRoles.push(
      {name: "Administrator", value: Role.ADMIN},
      {name: "Pracownik", value: Role.EMPLOYEE}
    )
  }

  register(event: AccountRegister) {
    this.accountRest.registerAccount(event).subscribe();
  }

  deleteAccount(accountId: string) {
    this.accountRest.deleteAccount(accountId).pipe(
      tap({
        complete: () => this.accountRest.getAccounts(this.searchParams, 0).subscribe(accounts => this.accountsPage = accounts)
      })
    ).subscribe();
  }

  change2FA($event: ChangeTwoFactor) {
    this.accountRest.change2FA($event).subscribe()
  }

  reset2FA(accountId: string) {
    this.accountRest.reset2FA(accountId).subscribe()
  }
}

import {ManagementComponent} from "./management/management.component";
import {AddAccountComponent} from "./add-account/add-account.component";
import {AccountsComponent} from "./accounts/accounts.component";
import {AccountSearchFormComponent} from "./account-search-form/account-search-form.component";

export const COMPONENTS = [
  ManagementComponent,
  AddAccountComponent,
  AccountsComponent,
  AccountSearchFormComponent
]

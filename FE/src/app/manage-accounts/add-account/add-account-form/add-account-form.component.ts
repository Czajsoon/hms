import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AccountRegister, RoleName} from "../../../models/account";
import {Observable} from "rxjs";

@Component({
  selector: 'hms-add-account-form',
  templateUrl: './add-account-form.component.html',
  styleUrls: ['./add-account-form.component.scss']
})
export class AddAccountFormComponent implements OnInit {
  @Input() availableRoles: RoleName[] = [];
  @Input() registerSuccess: Observable<boolean> = new Observable<boolean>();
  @Output() accountRegister: EventEmitter<AccountRegister> = new EventEmitter<AccountRegister>();
  @Output() formValid: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() closeModal: EventEmitter<boolean> = new EventEmitter<boolean>();
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.registerSuccess.subscribe(result => {
      if (result) {
        this.form.reset();
      }
    })
  }

  buildForm(): void {
    this.form = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
      firstName: [null, Validators.required],
      secondName: [null, Validators.required],
      role: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      isUsing2FA: [true]
    })
  }

  register() {
    this.accountRegister.emit(this.form.value);
  }

  changeVisibleModal() {
    this.closeModal.emit(false);
  }
}

import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {AccountRegister, RoleName} from "../../models/account";
import {FormGroup} from "@angular/forms";
import {AccountRestService} from "../../rest-services/account-rest.service";
import {tap} from "rxjs";

@Component({
  selector: 'hms-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.scss']
})
export class AddAccountComponent implements OnInit {
  @Input() availableRoles: RoleName[] = [];
  formValid: boolean;
  displayModal: boolean = false;
  registerSuccess: EventEmitter<boolean> = new EventEmitter<boolean>();
  form: FormGroup;

  constructor(private accountRest: AccountRestService) {
  }

  ngOnInit(): void {
  }

  register(event: AccountRegister) {
    this.accountRest.registerAccount(event).pipe(
      tap({
        complete: () => this.registerSuccess.emit(true),
        error: () => this.registerSuccess.emit(false)
      })
    ).subscribe()
  }

  changeVisibleModal() {
    this.displayModal = !this.displayModal;
  }
}

import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import {AccountRestService} from "../../rest-services/account-rest.service";
import {MustMatch} from "./MustMatch";
import {AccountPasswordChange} from "../../models/account";

@Component({
  selector: 'hms-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  form: FormGroup;
  accountChangePasswordRequest: AccountPasswordChange={
    password: null,
    newPassword: null
  };



  constructor(private fb: FormBuilder, private accountRest: AccountRestService) {
    this.buildForm();
  }

  ngOnInit(): void {
  }

  buildForm(){
    this.form = this.fb.group({
      password: ['',Validators.required],
      newPassword: ['',[Validators.minLength(8),Validators.maxLength(20),Validators.required]],
      confirmPassword:['',[Validators.minLength(8),Validators.maxLength(20), Validators.required]]
    },{validator: MustMatch('newPassword','confirmPassword')});
  }
  submit(){
    this.accountChangePasswordRequest.password=this.form.getRawValue().password;
    this.accountChangePasswordRequest.newPassword=this.form.getRawValue().newPassword;
    this.accountRest.changePassword(this.accountChangePasswordRequest).subscribe();
  }






}

export interface AccountDetail {
  firstName: string,
  secondName: string,
  role: Role;
  isUsing2FA: boolean;
}

export interface AccountDetailsPage {
  totalElements: number;
  totalPages: number;
  accounts: AccountDetails[];
}

export interface AccountRegister {
  password: string;
  username: string;
  firstName: string;
  secondName: string;
  role: Role;
}

export interface AccountDetails {
  id: string;
  username: string;
  firstName: string;
  secondName: string;
  role: Role;
  isUsing2FA: boolean;
}

export interface AccountPasswordChange{
  password: string,
  newPassword: string
}

export enum Role {
  ADMIN = "ADMIN",
  EMPLOYEE = "EMPLOYEE"
}

export interface RoleNames {
  ADMIN: "Administrator",
  EMPLOYEE: "Pracownik"
}

export interface RoleName {
  name: string;
  value: Role;
}




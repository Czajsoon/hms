export interface TwoFactorAccountConfig {
  isUsing2FA: boolean;
  is2FaConfigured: boolean;
}

export interface ChangeTwoFactor {
  value: boolean;
  accountId: string;
}

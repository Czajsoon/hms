import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginRestService} from "../rest-services/login-rest.service";
import {TokenHandlerService} from "../shared/token-handler/token-handler.service";

@Component({
  selector: 'hms-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public passwordVisibility: boolean = false;
  public loginForm: FormGroup;

  constructor(private fb: FormBuilder,
              public tokenHandlerService: TokenHandlerService,
              private loginRest: LoginRestService
  ) {
    this.buildLoginForm()
  }

  ngOnInit(): void {
  }

  private buildLoginForm() {
    this.loginForm = this.fb.group({
      login: [null, Validators.required],
      password: [null, Validators.required]
    })
  }

  changePasswordVisibility(): void {
    this.passwordVisibility = !this.passwordVisibility;
  }

  login() {
    this.loginRest.login(this.loginForm.value).subscribe()
  }
}

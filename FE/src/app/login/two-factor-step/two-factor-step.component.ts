import {Component, OnInit} from '@angular/core';
import {TokenHandlerService} from "../../shared/token-handler/token-handler.service";
import {AccountRestService} from "../../rest-services/account-rest.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'hms-two-factor-step',
  templateUrl: './two-factor-step.component.html',
  styleUrls: ['./two-factor-step.component.scss']
})
export class TwoFactorStepComponent implements OnInit {
  qrCode: any;
  form: FormGroup

  constructor(public tokenHandlerService: TokenHandlerService,
              private accountRestService: AccountRestService,
              private fb: FormBuilder) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.loadQrCode();
  }

  buildForm() {
    this.form = this.fb.group({
      code: [null, Validators.required]
    })
  }

  loadQrCode() {
    if (!this.tokenHandlerService.twoFA.is2FaConfigured) {
      this.accountRestService.getQrCode().subscribe((qrcode: Blob) => {
        let reader = new FileReader();
        reader.onload = () => {
          this.qrCode = reader.result as string;
        }
        reader.readAsDataURL(qrcode);
      });
    }
  }

  logout() {
    this.tokenHandlerService.logout()
  }

  login() {
    this.accountRestService.validateTwoFaCode(this.form.value.code, this.tokenHandlerService).subscribe()
  }
}

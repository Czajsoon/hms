import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {ResponseHandlerService} from "../shared/exceptions-handlers/response-handler.service";
import {Observable} from "rxjs";
import {AddRoomDTO, RoomDetailsPage, RoomStatus} from "../models/room";

@Injectable({
  providedIn: 'root'
})
export class RoomRestService {

  constructor(private http: HttpClient, private handler: ResponseHandlerService) { }

  public getRooms(params: any, page: number): Observable<RoomDetailsPage> {
    return this.http.get<RoomDetailsPage>("/services-rest/rooms/all", {
      params: RoomRestService.mapFindRoomParams(params, page)
    }).pipe(
      this.handler.handleError()
    )
  }

  private static mapFindRoomParams(params: any, page: number) {
    let httpParams = new HttpParams();
    if (params) {
      httpParams = params.balconyAvailable ? httpParams.append("balconyAvailable", params.balconyAvailable) : httpParams;
      httpParams = params.level ? httpParams.append("level", params.level) : httpParams;
      httpParams = params.roomStatus ? httpParams.append("roomStatus", params.roomStatus) : httpParams;
      httpParams = params.pricePerNight ? httpParams.append("pricePerNight", params.pricePerNight) : httpParams;
      httpParams = params.bedsNumber ? httpParams.append("bedsNumber", params.bedsNumber) : httpParams;
      httpParams = page ? httpParams.append("page", page) : httpParams;
      return httpParams;
    } else return httpParams;
  }

  public addRoom(room: AddRoomDTO) {
    return this.http.post("/services-rest/rooms/add", room).pipe(this.handler.handleError())
  }

  public updateRoomStatus(roomId: string, status: RoomStatus) {
    return this.http.patch(`/services-rest/rooms/status/${roomId}/${status}`, null).pipe(
      this.handler.handle()
    );
  }

}

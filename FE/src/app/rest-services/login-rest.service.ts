import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ResponseHandlerService} from "../shared/exceptions-handlers/response-handler.service";
import {TokenHandlerService} from "../shared/token-handler/token-handler.service";
import {switchMap, tap} from "rxjs";
import {TwoFactorAccountConfig} from "../models/TwoFactor";
import routing from "../../environments/routing";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class LoginRestService {

  constructor(private http: HttpClient,
              private handler: ResponseHandlerService,
              private tokenHandler: TokenHandlerService,
              private router: Router) {
  }

  login(body: any) {
    return this.http.post("/services-rest/authenticate/login", body).pipe(
      this.handler.handleError(),
      this.tokenHandler.handleToken(),
      switchMap(() => {
        return this.getTwoFactorAccountConfig();
      })
    );
  }

  getTwoFactorAccountConfig() {
    return this.http.get<TwoFactorAccountConfig>("/services-rest/authenticate/two-factor/config").pipe(
      this.handler.handleError(),
      tap({
        next: (accountConfig: TwoFactorAccountConfig) => {
          this.tokenHandler.twoFA = accountConfig;
          if (!accountConfig.isUsing2FA) {
            this.tokenHandler.twoFaValid = true;
            this.router.navigate([routing.rooms])
          }
        }
      })
    )
  }

  getSimpleTwoFactorAccountConfig() {
    return this.http.get<TwoFactorAccountConfig>("/services-rest/authenticate/two-factor/config").pipe(
      this.handler.handleError()
    )
  }
}

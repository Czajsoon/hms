import {Component, OnInit} from '@angular/core';
import {PrimeNGConfig} from "primeng/api";
import {NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from "@angular/router";
import routing from "../environments/routing";
import {routes} from "./app-routing.module";
import {LocationStrategy} from "@angular/common";
import {LoadingService} from "./shared/loading/loading.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'HotelManagementSystemGui';
  loading$ = this.loader.loading$;

  constructor(private primengConfig: PrimeNGConfig,
              public router: Router,
              private location: LocationStrategy,
              private loader: LoadingService) {
  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.router.events.subscribe((routerEvent) => this.checkRouterEvent(routerEvent))
  }


  displayNavbar(): boolean {
    return this.router.isActive(routing.login, true) || this.isRoutingAvailable();
  }

  private isRoutingAvailable(): boolean {
    return routes.find(routePath => routePath.path === this.location.path().substring(1)) === undefined
  }

  private checkRouterEvent(routerEvent) {
    if (routerEvent instanceof NavigationStart) {
      this.loader.show();
    } else if (routerEvent instanceof NavigationEnd ||
      routerEvent instanceof NavigationCancel ||
      routerEvent instanceof NavigationError) {
      this.loader.hide();
    }
  }
}

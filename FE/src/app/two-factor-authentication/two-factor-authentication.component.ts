import { Component, OnInit } from '@angular/core';
import {AccountRestService} from "../rest-services/account-rest.service";
import QRCode from 'qrcode'

@Component({
  selector: 'hms-two-factor-authentication',
  templateUrl: './two-factor-authentication.component.html',
  styleUrls: ['./two-factor-authentication.component.scss']
})
export class TwoFactorAuthenticationComponent implements OnInit {

  qrCode:any;

  constructor(private accountRestService: AccountRestService) { }

  ngOnInit(): void {
   this.accountRestService.getQrCode().subscribe((qrcode: Blob)=>{
     let reader = new FileReader();
     reader.onload= ()=>{
       this.qrCode= reader.result as string;
     }
     reader.readAsDataURL(qrcode);
   });

  }

}

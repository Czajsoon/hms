import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "./login/login.component";
import routing from "../environments/routing";
import urls from "../environments/urls";
import {RoomsComponent} from "./hotel-rooms/room/rooms.component";
import {AuthGuard} from "./shared/guards/auth.guard";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {ManagementComponent} from "./manage-accounts/management/management.component";
import {AdminGuard} from "./shared/guards/admin.guard";
import {TwoFactorAuthenticationComponent} from "./two-factor-authentication/two-factor-authentication.component";
import {ChangePasswordComponent} from "./manage-accounts/change-password/change-password.component";

export const routes: Routes = [
  {path: "", pathMatch: "full", redirectTo: urls.login},
  {path: routing.login, component: LoginComponent},
  {path: routing.rooms, component: RoomsComponent, canActivate: [AuthGuard]},
  {path: routing.management, component: ManagementComponent, canActivate: [AuthGuard, AdminGuard]},
  {path: routing.twoFactorAuth, component:TwoFactorAuthenticationComponent, canActivate:[AuthGuard]},
  {path: routing.changePassword, component: ChangePasswordComponent, canActivate: [AuthGuard]},
  {path: "**", component: PageNotFoundComponent, canActivate: [AuthGuard]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [AuthGuard],
  exports: [RouterModule]
})
export class AppRoutingModule { }

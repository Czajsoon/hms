import {Component} from '@angular/core';
import {LocationStrategy} from "@angular/common";

@Component({
  selector: 'hms-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent {
  constructor(private location: LocationStrategy) {
  }

  goBack() {
    this.location.back();
  }
}

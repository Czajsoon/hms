import {Component, OnInit} from '@angular/core';
import routing from "../../environments/routing";
import {TokenHandlerService} from "../shared/token-handler/token-handler.service";
import {Router} from "@angular/router";

@Component({
  selector: 'hms-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  routing: any = routing;

  constructor(public tokenHandlerService: TokenHandlerService) {
  }

  ngOnInit(): void {
  }

  protected readonly Router = Router;
}
